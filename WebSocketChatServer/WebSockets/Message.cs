﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSocketChatServer.WebSockets;

namespace WebSocketChatServer.WebSockets
{
    public enum StatusMessage
    {
        New,
        Accept,
        Read
    }

    public class Message
    {
        public string MessageType { get; set; }
        public StatusMessage MessageStatus { get; set; }
        public bool IsAccounter { get; set; }
        public string MessageText { get; set; }
        public Guid DialogId { get; set; }
        public Guid UserId { get; set; }
        public Guid AccounterId { get; set; }
        public string AccounterName { get; set; }
        public string UserName { get; set; }
        public string Date { get; set; }
    }
}
