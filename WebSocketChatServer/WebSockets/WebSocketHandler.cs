﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebSocketChatServer.ChatAPI;

namespace WebSocketChatServer.WebSockets
{
    public abstract class WebSocketHandler
    {
        protected WebSocketConnectionManager WebSocketConnectionManager { get; set; }

        public WebSocketHandler(WebSocketConnectionManager webSocketConnectionManager)
        {
            WebSocketConnectionManager = webSocketConnectionManager;
        }

        public virtual async Task OnConnected(Guid id,WebSocket socket)
        {
            WebSocketConnectionManager.AddSocket(id,socket);
        }

        public virtual async Task OnDisconnected(WebSocket socket)
        {
            await WebSocketConnectionManager.RemoveSocket(WebSocketConnectionManager.GetIdBySocket(socket));
        }

        public async Task SendMessageAsync(WebSocket socket, string message)
        {
            if (socket.State != WebSocketState.Open)
                return;

            await socket.SendAsync(buffer: new ArraySegment<byte>(array: Encoding.UTF8.GetBytes(message)),
                                   messageType: WebSocketMessageType.Text,
                                   endOfMessage: true,
                                   cancellationToken: CancellationToken.None);
        }
        //udalit nafig
        public async Task SendMessageAsync(Guid socketId, string message)
        {
            try
            {
                await SendMessageAsync(WebSocketConnectionManager.GetSocketById(socketId), message);
            }
            catch (Exception)
            {

            }

        }

        //smenit nazvaniya dla metodov
        public async Task SendMessageTo(List<Guid> sendId, Message message)
        {
            foreach (var id in sendId)
            {
                var sockets = WebSocketConnectionManager.GetSocketsById(id);
                //?? Enumerable.Empty<WebSocket>()
                if (sockets.Any())
                {
                    foreach (var socket in sockets)
                    {
                        if (socket.State == WebSocketState.Open)
                        {
                            await SendMessageAsync(socket, JsonConvert.SerializeObject(message));
                        }
                    }
                }
                else
                {
                    //poka dla clientov
                    if (message.IsAccounter.Equals(true))
                    {
                        await SendNotificationAsync(message);
                    }
                }
            }
        }
        //send async
        public async Task SendNotificationAsync(Message message)
        {
            var notify = new NotificationMobile();
            
            notify.SendPushAnswer(message);
        }

        public async Task SendMessageToAllAsync(string message)
        {
            foreach (var pair in WebSocketConnectionManager.GetSockets())
            {
                if (pair.Value.State == WebSocketState.Open)
                    await SendMessageAsync(pair.Value, message);
            }
        }

        public abstract Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer);
    }

    //public enum MessageType
    //{
    //    Text,
    //    ClientMethodInvocation,
    //    ConnectionEvent
    //}
}
