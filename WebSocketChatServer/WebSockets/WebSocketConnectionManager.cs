﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.WebSockets;
using System.Threading;

namespace WebSocketChatServer.WebSockets
{
    public class WebSocketConnectionManager
    {
        /// <summary>
        /// Потоко безопасная коллекция для предотвращения гонки потоков при запросах к коллекции. 
        /// </summary>
        private readonly BlockingCollection<KeyValuePair<Guid, WebSocket>> _webSockets = new BlockingCollection<KeyValuePair<Guid, WebSocket>>();

        /// <summary>
        /// Нахождение сокета по номеру пользователя.
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        /// <returns>Сокет.</returns>
        public WebSocket GetSocketById(Guid id)
        {
            return _webSockets.SingleOrDefault(x => x.Key == id).Value;
        }

        public List<WebSocket> GetSocketsById(Guid id)
        {
            return _webSockets.Where(x => x.Key.Equals(id)).Select(x => x.Value).ToList();
        }
        /// <summary>
        ///Функция для возврата всей коллекции.
        /// </summary>
        /// <returns>Возвращает коллекцию всех сокетов.</returns>
        public BlockingCollection<KeyValuePair<Guid, WebSocket>> GetSockets()
        {
            return _webSockets;
        }

        /// <summary>
        /// Нахождение пользователя по сокету. 
        /// </summary>
        /// <param name="socket">Сокет.</param>
        /// <returns>Идентификатор пользователя.</returns>
        public Guid GetIdBySocket(WebSocket socket)
        {
            return _webSockets.SingleOrDefault(x => x.Value == socket).Key;
        }

        /// <summary>
        /// Добавление соединения.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="socket">Сокет.</param>
        public void AddSocket(Guid id, WebSocket socket)
        {
            while (!_webSockets.TryAdd(new KeyValuePair<Guid, WebSocket>(id, socket))){ }
        }

        public async Task RemoveSocket(Guid id)
        {
            foreach (var socket in GetSocketsById(id))
            {
                var item = new KeyValuePair<Guid,WebSocket>(id,socket);

                _webSockets.TryTake(out item);

                if (socket != null)
                {
                    await socket.CloseAsync(closeStatus: WebSocketCloseStatus.NormalClosure,
                        statusDescription: "Closed by the WebSocketManager",
                        cancellationToken: CancellationToken.None).ConfigureAwait(false);
                }
            } 
        }

    }
}
