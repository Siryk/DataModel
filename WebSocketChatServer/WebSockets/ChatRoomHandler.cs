﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using WebSocketChatServer.ChatAPI;
using WebSocketChatServer.Core;
using WebSocketChatServer.Entities;
using Newtonsoft.Json;
namespace WebSocketChatServer.WebSockets
{
    public class ChatRoomHandler : WebSocketHandler
    {
        private static ChatManager manager = new ChatManager(new UnitOfWork(new SauriChatContext()));

        public ChatRoomHandler(WebSocketConnectionManager webSocketConnectionManager) : base(webSocketConnectionManager)
        {
        }

        public override async Task OnConnected(Guid id, WebSocket socket)
        {
            await base.OnConnected(id, socket);
            //otpravka soobweniya
            //Message message = new Message();
            //message.MessageType = "status";
            //message.UserId = id;

            //await SendMessageToAllAsync(JsonConvert.SerializeObject<Message>(message));
        }

        public override async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            var message = JsonConvert.DeserializeObject<Message>(Encoding.UTF8.GetString(buffer, 0, result.Count));
            var id = WebSocketConnectionManager.GetIdBySocket(socket);

            Guid idDialog;
            Guid idFrom;
            //task ????
            if (message.IsAccounter.Equals(false))
            {
                await Task.Factory.StartNew(() =>
                {
                    idDialog = manager.GetDialog(id);
                    idFrom = manager.AccounterIdInDialog(idDialog);
                    message.UserId = id;
                    message.DialogId = idDialog;
                }); //po4itat
            }
            else
            {
                await Task.Factory.StartNew(() =>
                {
                    idDialog = manager.GetDialog(message.UserId, id);
                    idFrom = message.UserId;
                    message.AccounterName = message.AccounterName;
                });
            }
            //govno
            var time = DateTime.Now;
            message.Date = time.ToString("d MMMMM H:mm");
            await manager.CreateMessage(idDialog, message.IsAccounter, message.MessageText, StatusMessage.Accept.ToString(),time); //zdes nygno accept rewit kak ono s new na accept menayetsa i na read.
            //send Async?check

            //moget try ostavit
            await SendMessageTo(new List<Guid>() {id, idFrom}, message);

            //await SendMessageTo(idAccountant, "wowowowkakaka");
        }

        //otpravka po id dialoga rassparalelit
        //public override async Task PushNotification(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        //{
        //    //var message = JsonConvert.DeserializeObject<Message>(Encoding.UTF8.GetString(buffer, 0, result.Count));
        //    //var id = WebSocketConnectionManager.GetIdBySocket(socket);
        //}

    }
}
