﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebSocketChatServer.Repository;

namespace WebSocketChatServer.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;

        public UnitOfWork(DbContext context)
        {
            _context = context;
            Users = new UsersRepository(_context);
            AllMessages = new MessagesRepository(_context);
            Dialogs = new DialogRepository(_context);
            AllAccountants = new AccountantsRepository(_context);
            Pool = new AccountantPoolRepository(_context);
            BridgeRepository = new UsersToAccountauntRepository(_context);
        }

        public IUsersRepository Users { get; private set; }
        public IMessagesRepository AllMessages { get; private set; }
        public IDialogRepository Dialogs { get; private set; }
        public IAccountantsRepository AllAccountants { get; private set; }
        public IAccountantPoolRepository Pool { get; private set; }
        public IUsersToAccountauntRepository BridgeRepository { get; private set; }


        public int Complete()
        {
            return _context.SaveChanges();
        }

        public Task<int> CompleteAsync()
        {
            return _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
