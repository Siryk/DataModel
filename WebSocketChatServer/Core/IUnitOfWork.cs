﻿using System;
using System.Threading.Tasks;
using WebSocketChatServer.Repository;

namespace WebSocketChatServer.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IUsersRepository Users { get; }
        IMessagesRepository AllMessages { get; }
        IDialogRepository Dialogs { get; }
        IAccountantsRepository AllAccountants { get; }
        IAccountantPoolRepository Pool { get; }
        IUsersToAccountauntRepository BridgeRepository {get; }
        int Complete();
        Task<int> CompleteAsync();
    }
}
