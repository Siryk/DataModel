﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebSocketChatServer.WebSockets;

namespace WebSocketChatServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors(options =>
            {
                options.AddPolicy("chatpolice",
                    policy => policy.AllowAnyOrigin().AllowAnyHeader()
                        .AllowAnyMethod()); //WithOrigins("http://localhost:21314"));
            });

            //services.AddMvc();
            services.AddMvc(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("chatpolice"));
            });

            //services.AddCors();

            services.AddWebSocketManager();

           
            //services.AddDbContext<sauri_v2Entities>(options => options.UseSqlServer(Configuration.GetConnectionString("sauri_v2Entities")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var websocketOption = new WebSocketOptions()
            {
                KeepAliveInterval = TimeSpan.FromSeconds(60),
                ReceiveBufferSize = 4 * 1024
            };

            app.UseWebSockets(websocketOption);

            app.MapWebSocketManager("/chat", serviceProvider.GetService<ChatRoomHandler>());
            //app.UseCors(builder => builder.AllowAnyOrigin()
            //    .AllowAnyHeader()
            //    .AllowAnyMethod());
            app.UseMvc();
        }
    }
}
