﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocketChatServer.ViewModel
{
    public class ContactModel
    {
        public string UserName { get; set; }
        public string Phone { get; set; }
        public Guid UserId { get; set; }
    }
}
