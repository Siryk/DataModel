﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocketChatServer.ViewModel
{
    public class DialogRequestModel
    {
        public Guid DialogId { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public Guid UserId { get; set; }
        //public List<UsersMessages> MessagesesList { get; set; } = new List<UsersMessages>();
        public int Count { get; set; }

    }
}
