﻿using System;

namespace WebSocketChatServer.ViewModel
{
    public class MessageModel
    {
        public Guid MessageId { get; set; }
        public Guid DialogId { get; set; }
        public string StatusMessages { get; set; }
        public string Date { get; set; }
        public bool AuthorStatus { get; set; }
        public string MessageText { get; set; }
        public string NameInterviewee { get; set; }
    }
}