﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class SjobjectMethodParams
    {
        public SjobjectMethodParams()
        {
            AcUserKpiparams = new HashSet<AcUserKpiparams>();
        }

        public Guid Id { get; set; }
        public Guid SubjectMethodId { get; set; }
        public string ParamName { get; set; }
        public string ParamCaption { get; set; }
        public bool RequiredParam { get; set; }
        public bool? DefaultParam { get; set; }
        public string DefaultValue { get; set; }
        public string ParamType { get; set; }
        public string DatabaseObjectType { get; set; }
        public string DatabaseObjectName { get; set; }

        public SjobjectMethods SubjectMethod { get; set; }
        public ICollection<AcUserKpiparams> AcUserKpiparams { get; set; }
    }
}
