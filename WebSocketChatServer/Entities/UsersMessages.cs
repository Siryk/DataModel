﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WebSocketChatServer.Entities
{
    public partial class UsersMessages
    {
        public Guid MessageId { get; set; }
        public Guid DialogId { get; set; }
        public string StatusMessages { get; set; }
        public DateTime Date { get; set; }
        public bool AuthorStatus { get; set; }
        public string MessageText { get; set; }
        [JsonIgnore]
        public AcUserDialogs Dialog { get; set; }
    }
}
