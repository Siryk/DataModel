﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class UsersToAccountants
    {
        public Guid AccountantId { get; set; }
        public Guid UserId { get; set; }

        public Accountants Accountant { get; set; }
        public Users User { get; set; }
    }
}
