﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class AccountUserRequests
    {
        public AccountUserRequests()
        {
            AccountUserRequestHypothesis = new HashSet<AccountUserRequestHypothesis>();
        }

        public Guid Id { get; set; }
        public Guid? AccountUserId { get; set; }
        public string UserRequest { get; set; }
        public string SpeechFileLink { get; set; }
        public DateTime? СreationDateTime { get; set; }
        public string UserResponse { get; set; }
        public bool? PerformanceIndicator { get; set; }
        public string AccountUserResponse { get; set; }
        public Guid? DialogId { get; set; }

        public AcUserDialogs Dialog { get; set; }
        public ICollection<AccountUserRequestHypothesis> AccountUserRequestHypothesis { get; set; }
    }
}
