﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class AcDatabaseIndexedContent
    {
        public Guid Id { get; set; }
        public Guid DatabaseItemId { get; set; }
        public Guid AccountDatabaseId { get; set; }
        public int? DatabaseItemType { get; set; }
        public string DatabaseItemName { get; set; }
        public int? Order { get; set; }
        public string ItemValueWord { get; set; }
        public Guid? ItemValueId { get; set; }
        public Guid LexicalClassId { get; set; }
        public string DatabaseItemCode { get; set; }
    }
}
