﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class SjobjectMethods
    {
        public SjobjectMethods()
        {
            AcUsersKpis = new HashSet<AcUsersKpis>();
            SjobjectMethodDataBaseSerialization = new HashSet<SjobjectMethodDataBaseSerialization>();
            SjobjectMethodParams = new HashSet<SjobjectMethodParams>();
        }

        public Guid Id { get; set; }
        public Guid SubjectObjectId { get; set; }
        public string MethodName { get; set; }
        public bool? DefaultMethod { get; set; }

        public SubjectAreaObjects SubjectObject { get; set; }
        public ICollection<AcUsersKpis> AcUsersKpis { get; set; }
        public ICollection<SjobjectMethodDataBaseSerialization> SjobjectMethodDataBaseSerialization { get; set; }
        public ICollection<SjobjectMethodParams> SjobjectMethodParams { get; set; }
    }
}
