﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class AcUserKpiparams
    {
        public Guid Id { get; set; }
        public Guid UsersKpiid { get; set; }
        public Guid ObjectMethodParamId { get; set; }
        public string ParamValue { get; set; }

        public SjobjectMethodParams ObjectMethodParam { get; set; }
        public AcUsersKpis UsersKpi { get; set; }
    }
}
