﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class EasterEggsResponses
    {
        public Guid Id { get; set; }
        public Guid SubjectMethodId { get; set; }
        public string Response { get; set; }
    }
}
