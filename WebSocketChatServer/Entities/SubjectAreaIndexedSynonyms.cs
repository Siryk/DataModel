﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class SubjectAreaIndexedSynonyms
    {
        public Guid Id { get; set; }
        public Guid MetadataItemId { get; set; }
        public Guid? LexicalClassId { get; set; }
        public int? MetadataItemType { get; set; }
        public int? Order { get; set; }
        public string SynonymWord { get; set; }
        public Guid? SynonymousExpressionId { get; set; }
    }
}
