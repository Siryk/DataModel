﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class SjobjectMethodDataBaseSerialization
    {
        public Guid Id { get; set; }
        public Guid SubjectMethodId { get; set; }
        public Guid AccountDatabaseId { get; set; }
        public string ConfigurationName { get; set; }
        public string ConfigurationVersion { get; set; }
        public string Dbmethod { get; set; }
        public string DbtypeMethod { get; set; }
        public string DbmethodFormatResult { get; set; }
        public string DbmethodParam3 { get; set; }
        public string DbmethodParam2 { get; set; }
        public string DbmethodParam1 { get; set; }
        public string FormEmptyTemplateViewPath { get; set; }
        public string FormModelViewTemplatePath { get; set; }
        public string EmptyFormatResults { get; set; }
        public string HelpResult { get; set; }

        public SjobjectMethods SubjectMethod { get; set; }
    }
}
