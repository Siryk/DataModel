﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class Consultants
    {
        public Consultants()
        {
            Users = new HashSet<Users>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string CarrotId { get; set; }
        public bool Main { get; set; }

        public ICollection<Users> Users { get; set; }
    }
}
