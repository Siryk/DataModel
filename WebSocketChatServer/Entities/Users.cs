﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class Users
    {
        public Users()
        {
            AcUserDialogs = new HashSet<AcUserDialogs>();
            UsersToAccountants = new HashSet<UsersToAccountants>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string CarrotId { get; set; }
        public Guid? ConsultantId { get; set; }
        public string MobileNotifySettings { get; set; }

        public Consultants Consultant { get; set; }
        public ICollection<AcUserDialogs> AcUserDialogs { get; set; }
        public ICollection<UsersToAccountants> UsersToAccountants { get; set; }
    }
}
