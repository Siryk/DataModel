﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class SubjectAreaObjects
    {
        public SubjectAreaObjects()
        {
            AcUsersKpis = new HashSet<AcUsersKpis>();
            SjobjectMethods = new HashSet<SjobjectMethods>();
        }

        public Guid Id { get; set; }
        public string ObjectName { get; set; }

        public ICollection<AcUsersKpis> AcUsersKpis { get; set; }
        public ICollection<SjobjectMethods> SjobjectMethods { get; set; }
    }
}
