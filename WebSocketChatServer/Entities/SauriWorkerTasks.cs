﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class SauriWorkerTasks
    {
        public Guid Id { get; set; }
        public string SauriWorkerTaskId { get; set; }
        public string Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public short SauriWorkerId { get; set; }
        public string Comment { get; set; }

        public SauriWorkers SauriWorker { get; set; }
    }
}
