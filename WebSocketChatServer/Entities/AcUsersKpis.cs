﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class AcUsersKpis
    {
        public AcUsersKpis()
        {
            AcUserKpiparams = new HashSet<AcUserKpiparams>();
        }

        public Guid Id { get; set; }
        public Guid AccountUserId { get; set; }
        public Guid SubjectObjectId { get; set; }
        public Guid SubjectMethodId { get; set; }
        public string UserKpiresponse { get; set; }
        public string CustomQueryText { get; set; }
        public string CustomQueryView { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string Response { get; set; }

        public SjobjectMethods SubjectMethod { get; set; }
        public SubjectAreaObjects SubjectObject { get; set; }
        public ICollection<AcUserKpiparams> AcUserKpiparams { get; set; }
    }
}
