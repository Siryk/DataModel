﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class AcUserRequestHypothesisParams
    {
        public Guid Id { get; set; }
        public Guid RequestHypothesisId { get; set; }
        public Guid SjobjectMethodParamId { get; set; }
        public string ParamValue { get; set; }
        public int? SjparamMatchingPercentage { get; set; }

        public AccountUserRequestHypothesis RequestHypothesis { get; set; }
    }
}
