﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class AccounterPool
    {
        public Guid AccounterId { get; set; }

        public Accountants Accounter { get; set; }
    }
}
