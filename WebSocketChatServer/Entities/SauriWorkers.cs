﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class SauriWorkers
    {
        public SauriWorkers()
        {
            SauriWorkerTasks = new HashSet<SauriWorkerTasks>();
        }

        public short Id { get; set; }
        public short? Status { get; set; }
        public DateTime? Tick { get; set; }
        public short? BusyThreadsCount { get; set; }
        public short? MaxThreadsCount { get; set; }

        public ICollection<SauriWorkerTasks> SauriWorkerTasks { get; set; }
    }
}
