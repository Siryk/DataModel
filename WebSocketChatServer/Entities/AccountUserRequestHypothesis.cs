﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class AccountUserRequestHypothesis
    {
        public AccountUserRequestHypothesis()
        {
            AcUserRequestHypothesisParams = new HashSet<AcUserRequestHypothesisParams>();
        }

        public Guid Id { get; set; }
        public Guid AccountUserRequestId { get; set; }
        public Guid SjobjectId { get; set; }
        public Guid SjObjectMethodId { get; set; }
        public int? SjobjectMatchingPercentage { get; set; }
        public int? SjObjectMethodMatchingPercentage { get; set; }
        public int? HypothesisRating { get; set; }

        public AccountUserRequests AccountUserRequest { get; set; }
        public ICollection<AcUserRequestHypothesisParams> AcUserRequestHypothesisParams { get; set; }
    }
}
