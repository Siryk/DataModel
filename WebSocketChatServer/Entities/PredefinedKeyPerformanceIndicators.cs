﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class PredefinedKeyPerformanceIndicators
    {
        public Guid Id { get; set; }
        public Guid AccountUserId { get; set; }
        public Guid AccountUserRequestsId { get; set; }
    }
}
