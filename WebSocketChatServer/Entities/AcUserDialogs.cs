﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class AcUserDialogs
    {
        public AcUserDialogs()
        {
            AccountUserRequests = new HashSet<AccountUserRequests>();
            UsersMessages = new HashSet<UsersMessages>();
        }

        public Guid Id { get; set; }
        public string CarrotId { get; set; }
        public Guid AccountUserId { get; set; }
        public string CarrotUserId { get; set; }
        public string Status { get; set; }
        public Guid UserId { get; set; }
        public Guid? AccountantsId { get; set; }

        public Accountants Accountants { get; set; }
        public Users User { get; set; }
        public ICollection<AccountUserRequests> AccountUserRequests { get; set; }
        public ICollection<UsersMessages> UsersMessages { get; set; }
    }
}
