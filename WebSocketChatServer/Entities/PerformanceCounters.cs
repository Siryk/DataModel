﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class PerformanceCounters
    {
        public Guid Id { get; set; }
        public Guid AccountUserId { get; set; }
        public Guid? AccountUserRequestsId { get; set; }
        public string PerformanceCounterId { get; set; }
        public double PerformanceCounterValue { get; set; }
        public DateTime FixationDateTime { get; set; }
        public string UserAgent { get; set; }
    }
}
