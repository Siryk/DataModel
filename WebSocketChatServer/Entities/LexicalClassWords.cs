﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class LexicalClassWords
    {
        public Guid Id { get; set; }
        public Guid LexicalClassId { get; set; }
        public string Word { get; set; }

        public LexicalClasses LexicalClass { get; set; }
    }
}
