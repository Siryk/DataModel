﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebSocketChatServer.Entities
{
    public partial class SauriChatContext : DbContext
    {
        public virtual DbSet<Accountants> Accountants { get; set; }
        public virtual DbSet<AccounterPool> AccounterPool { get; set; }
        public virtual DbSet<AccountUserRequestHypothesis> AccountUserRequestHypothesis { get; set; }
        public virtual DbSet<AccountUserRequests> AccountUserRequests { get; set; }
        public virtual DbSet<AcDatabaseIndexedContent> AcDatabaseIndexedContent { get; set; }
        public virtual DbSet<AcUserDialogs> AcUserDialogs { get; set; }
        public virtual DbSet<AcUserKpiparams> AcUserKpiparams { get; set; }
        public virtual DbSet<AcUserRequestHypothesisParams> AcUserRequestHypothesisParams { get; set; }
        public virtual DbSet<AcUsersKpis> AcUsersKpis { get; set; }
        public virtual DbSet<Consultants> Consultants { get; set; }
        public virtual DbSet<EasterEggsResponses> EasterEggsResponses { get; set; }
        public virtual DbSet<LexicalClasses> LexicalClasses { get; set; }
        public virtual DbSet<LexicalClassWords> LexicalClassWords { get; set; }
        public virtual DbSet<PerformanceCounters> PerformanceCounters { get; set; }
        public virtual DbSet<PredefinedKeyPerformanceIndicators> PredefinedKeyPerformanceIndicators { get; set; }
        public virtual DbSet<SauriWorkers> SauriWorkers { get; set; }
        public virtual DbSet<SauriWorkerTasks> SauriWorkerTasks { get; set; }
        public virtual DbSet<SjobjectMethodDataBaseSerialization> SjobjectMethodDataBaseSerialization { get; set; }
        public virtual DbSet<SjobjectMethodParams> SjobjectMethodParams { get; set; }
        public virtual DbSet<SjobjectMethods> SjobjectMethods { get; set; }
        public virtual DbSet<SubjectAreaIndexedSynonyms> SubjectAreaIndexedSynonyms { get; set; }
        public virtual DbSet<SubjectAreaObjects> SubjectAreaObjects { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<UsersMessages> UsersMessages { get; set; }
        public virtual DbSet<UsersToAccountants> UsersToAccountants { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"data source=10.20.1.141;initial catalog=sauri_v2;persist security info=True;user id=billing;password=Oen5839wsr;MultipleActiveResultSets=True;App=EntityFramework&quot;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accountants>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<AccounterPool>(entity =>
            {
                entity.HasKey(e => e.AccounterId);

                entity.Property(e => e.AccounterId).ValueGeneratedNever();

                entity.HasOne(d => d.Accounter)
                    .WithOne(p => p.AccounterPool)
                    .HasForeignKey<AccounterPool>(d => d.AccounterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Accounter__Accou__1F98B2C1");
            });

            modelBuilder.Entity<AccountUserRequestHypothesis>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountUserRequestId).HasColumnName("AccountUserRequestID");

                entity.Property(e => e.SjObjectMethodId).HasColumnName("SjObjectMethodID");

                entity.Property(e => e.SjobjectId).HasColumnName("SJObjectID");

                entity.Property(e => e.SjobjectMatchingPercentage).HasColumnName("SJObjectMatchingPercentage");

                entity.HasOne(d => d.AccountUserRequest)
                    .WithMany(p => p.AccountUserRequestHypothesis)
                    .HasForeignKey(d => d.AccountUserRequestId)
                    .HasConstraintName("FK__AccountUs__Accou__1367E606");
            });

            modelBuilder.Entity<AccountUserRequests>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountUserId).HasColumnName("AccountUserID");

                entity.Property(e => e.SpeechFileLink).HasMaxLength(200);

                entity.Property(e => e.UserRequest).HasMaxLength(2000);

                entity.Property(e => e.UserResponse).HasColumnType("text");

                entity.Property(e => e.СreationDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Dialog)
                    .WithMany(p => p.AccountUserRequests)
                    .HasForeignKey(d => d.DialogId)
                    .HasConstraintName("FK__AccountUs__Dialo__6D0D32F4");
            });

            modelBuilder.Entity<AcDatabaseIndexedContent>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountDatabaseId).HasColumnName("AccountDatabaseID");

                entity.Property(e => e.DatabaseItemCode).HasMaxLength(200);

                entity.Property(e => e.DatabaseItemId).HasColumnName("DatabaseItemID");

                entity.Property(e => e.DatabaseItemName).HasMaxLength(200);

                entity.Property(e => e.ItemValueId).HasColumnName("ItemValueID");

                entity.Property(e => e.ItemValueWord).HasMaxLength(200);

                entity.Property(e => e.LexicalClassId).HasColumnName("LexicalClassID");
            });

            modelBuilder.Entity<AcUserDialogs>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountUserId).HasColumnName("AccountUserID");

                entity.Property(e => e.AccountantsId).HasColumnName("AccountantsID");

                entity.Property(e => e.CarrotId)
                    .IsRequired()
                    .HasColumnName("CarrotID")
                    .HasMaxLength(100);

                entity.Property(e => e.CarrotUserId)
                    .IsRequired()
                    .HasColumnName("CarrotUserID")
                    .HasMaxLength(100);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Accountants)
                    .WithMany(p => p.AcUserDialogs)
                    .HasForeignKey(d => d.AccountantsId)
                    .HasConstraintName("FK_AccDialogs");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AcUserDialogs)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UsersDialogs");
            });

            modelBuilder.Entity<AcUserKpiparams>(entity =>
            {
                entity.ToTable("AcUserKPIParams");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ObjectMethodParamId).HasColumnName("ObjectMethodParamID");

                entity.Property(e => e.ParamValue).HasMaxLength(200);

                entity.Property(e => e.UsersKpiid).HasColumnName("UsersKPIId");

                entity.HasOne(d => d.ObjectMethodParam)
                    .WithMany(p => p.AcUserKpiparams)
                    .HasForeignKey(d => d.ObjectMethodParamId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AcAcUserK__Objec__48CFD27E");

                entity.HasOne(d => d.UsersKpi)
                    .WithMany(p => p.AcUserKpiparams)
                    .HasForeignKey(d => d.UsersKpiid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AcAcUserK__Users__49C3F6B7");
            });

            modelBuilder.Entity<AcUserRequestHypothesisParams>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ParamValue).HasMaxLength(200);

                entity.Property(e => e.RequestHypothesisId).HasColumnName("RequestHypothesisID");

                entity.Property(e => e.SjobjectMethodParamId).HasColumnName("SJObjectMethodParamID");

                entity.Property(e => e.SjparamMatchingPercentage).HasColumnName("SJParamMatchingPercentage");

                entity.HasOne(d => d.RequestHypothesis)
                    .WithMany(p => p.AcUserRequestHypothesisParams)
                    .HasForeignKey(d => d.RequestHypothesisId)
                    .HasConstraintName("FK__AcUserReq__Reque__145C0A3F");
            });

            modelBuilder.Entity<AcUsersKpis>(entity =>
            {
                entity.ToTable("AcUsersKPIs");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountUserId).HasColumnName("AccountUserID");

                entity.Property(e => e.CustomQueryText).HasColumnType("text");

                entity.Property(e => e.CustomQueryView).HasMaxLength(200);

                entity.Property(e => e.LastUpdateDate).HasColumnType("datetime");

                entity.Property(e => e.SubjectMethodId).HasColumnName("SubjectMethodID");

                entity.Property(e => e.SubjectObjectId).HasColumnName("SubjectObjectID");

                entity.Property(e => e.UserKpiresponse)
                    .HasColumnName("UserKPIResponse")
                    .HasColumnType("text");

                entity.HasOne(d => d.SubjectMethod)
                    .WithMany(p => p.AcUsersKpis)
                    .HasForeignKey(d => d.SubjectMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AcAcUsers__Subje__440B1D61");

                entity.HasOne(d => d.SubjectObject)
                    .WithMany(p => p.AcUsersKpis)
                    .HasForeignKey(d => d.SubjectObjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AcAcUsers__Subje__4316F928");
            });

            modelBuilder.Entity<Consultants>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CarrotId)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<EasterEggsResponses>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Response).HasMaxLength(200);

                entity.Property(e => e.SubjectMethodId).HasColumnName("SubjectMethodID");
            });

            modelBuilder.Entity<LexicalClasses>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.IndexCreationTime).HasColumnType("datetime");

                entity.Property(e => e.LexicalClass).HasMaxLength(200);

                entity.Property(e => e.ShortLexicalClass).HasMaxLength(200);
            });

            modelBuilder.Entity<LexicalClassWords>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.LexicalClassId).HasColumnName("LexicalClassID");

                entity.Property(e => e.Word).HasMaxLength(200);

                entity.HasOne(d => d.LexicalClass)
                    .WithMany(p => p.LexicalClassWords)
                    .HasForeignKey(d => d.LexicalClassId)
                    .HasConstraintName("FK__LexicalCl__Lexic__173876EA");
            });

            modelBuilder.Entity<PerformanceCounters>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.FixationDateTime).HasColumnType("datetime");

                entity.Property(e => e.PerformanceCounterId)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.UserAgent).HasColumnType("nchar(75)");
            });

            modelBuilder.Entity<PredefinedKeyPerformanceIndicators>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<SauriWorkers>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Tick).HasColumnType("datetime");
            });

            modelBuilder.Entity<SauriWorkerTasks>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Comment).HasMaxLength(100);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.EditDate).HasColumnType("datetime");

                entity.Property(e => e.SauriWorkerTaskId)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.HasOne(d => d.SauriWorker)
                    .WithMany(p => p.SauriWorkerTasks)
                    .HasForeignKey(d => d.SauriWorkerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SauriWork__Sauri__5FB337D6");
            });

            modelBuilder.Entity<SjobjectMethodDataBaseSerialization>(entity =>
            {
                entity.ToTable("SJObjectMethodDataBaseSerialization");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountDatabaseId).HasColumnName("AccountDatabaseID");

                entity.Property(e => e.ConfigurationName).HasMaxLength(100);

                entity.Property(e => e.ConfigurationVersion).HasMaxLength(50);

                entity.Property(e => e.Dbmethod)
                    .HasColumnName("DBMethod")
                    .HasMaxLength(200);

                entity.Property(e => e.DbmethodFormatResult)
                    .HasColumnName("DBMethodFormatResult")
                    .HasMaxLength(2000);

                entity.Property(e => e.DbmethodParam1)
                    .HasColumnName("DBMethodParam1")
                    .HasColumnType("ntext");

                entity.Property(e => e.DbmethodParam2)
                    .HasColumnName("DBMethodParam2")
                    .HasMaxLength(4000);

                entity.Property(e => e.DbmethodParam3)
                    .HasColumnName("DBMethodParam3")
                    .HasMaxLength(200);

                entity.Property(e => e.DbtypeMethod)
                    .HasColumnName("DBTypeMethod")
                    .HasMaxLength(10);

                entity.Property(e => e.EmptyFormatResults).HasColumnType("ntext");

                entity.Property(e => e.FormEmptyTemplateViewPath).HasMaxLength(1000);

                entity.Property(e => e.FormModelViewTemplatePath).HasMaxLength(1000);

                entity.Property(e => e.HelpResult).HasColumnType("ntext");

                entity.Property(e => e.SubjectMethodId).HasColumnName("SubjectMethodID");

                entity.HasOne(d => d.SubjectMethod)
                    .WithMany(p => p.SjobjectMethodDataBaseSerialization)
                    .HasForeignKey(d => d.SubjectMethodId)
                    .HasConstraintName("FK__SJObjectM__Subje__09DE7BCC");
            });

            modelBuilder.Entity<SjobjectMethodParams>(entity =>
            {
                entity.ToTable("SJObjectMethodParams");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DatabaseObjectName).HasMaxLength(200);

                entity.Property(e => e.DatabaseObjectType).HasMaxLength(200);

                entity.Property(e => e.DefaultValue).HasMaxLength(200);

                entity.Property(e => e.ParamCaption).HasMaxLength(200);

                entity.Property(e => e.ParamName).HasMaxLength(200);

                entity.Property(e => e.ParamType).HasMaxLength(200);

                entity.Property(e => e.SubjectMethodId).HasColumnName("SubjectMethodID");

                entity.HasOne(d => d.SubjectMethod)
                    .WithMany(p => p.SjobjectMethodParams)
                    .HasForeignKey(d => d.SubjectMethodId)
                    .HasConstraintName("FK__SJObjectM__Subje__15502E78");
            });

            modelBuilder.Entity<SjobjectMethods>(entity =>
            {
                entity.ToTable("SJObjectMethods");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.MethodName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.SubjectObjectId).HasColumnName("SubjectObjectID");

                entity.HasOne(d => d.SubjectObject)
                    .WithMany(p => p.SjobjectMethods)
                    .HasForeignKey(d => d.SubjectObjectId)
                    .HasConstraintName("FK__SJObjectM__Subje__164452B1");
            });

            modelBuilder.Entity<SubjectAreaIndexedSynonyms>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.LexicalClassId).HasColumnName("LexicalClassID");

                entity.Property(e => e.MetadataItemId).HasColumnName("MetadataItemID");

                entity.Property(e => e.SynonymWord).HasMaxLength(200);

                entity.Property(e => e.SynonymousExpressionId).HasColumnName("SynonymousExpressionID");
            });

            modelBuilder.Entity<SubjectAreaObjects>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ObjectName).HasMaxLength(200);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CarrotId)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.ConsultantId).HasColumnName("ConsultantID");

                entity.Property(e => e.MobileNotifySettings).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.Consultant)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.ConsultantId)
                    .HasConstraintName("FK__Users__Consultan__7E37BEF6");
            });

            modelBuilder.Entity<UsersMessages>(entity =>
            {
                entity.HasKey(e => e.MessageId);

                entity.Property(e => e.MessageId).ValueGeneratedNever();

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.HasOne(d => d.Dialog)
                    .WithMany(p => p.UsersMessages)
                    .HasForeignKey(d => d.DialogId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UsersMess__Autho__19DFD96B");
            });

            modelBuilder.Entity<UsersToAccountants>(entity =>
            {
                entity.HasKey(e => new { e.AccountantId, e.UserId });

                entity.HasOne(d => d.Accountant)
                    .WithMany(p => p.UsersToAccountants)
                    .HasForeignKey(d => d.AccountantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UsersToAc__Accou__17036CC0");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UsersToAccountants)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UsersToAc__UserI__17F790F9");
            });
        }
    }
}
