﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class LexicalClasses
    {
        public LexicalClasses()
        {
            LexicalClassWords = new HashSet<LexicalClassWords>();
        }

        public Guid Id { get; set; }
        public string LexicalClass { get; set; }
        public DateTime? IndexCreationTime { get; set; }
        public string ShortLexicalClass { get; set; }

        public ICollection<LexicalClassWords> LexicalClassWords { get; set; }
    }
}
