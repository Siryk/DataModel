﻿using System;
using System.Collections.Generic;

namespace WebSocketChatServer.Entities
{
    public partial class Accountants
    {
        public Accountants()
        {
            AcUserDialogs = new HashSet<AcUserDialogs>();
            UsersToAccountants = new HashSet<UsersToAccountants>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public AccounterPool AccounterPool { get; set; }
        public ICollection<AcUserDialogs> AcUserDialogs { get; set; }
        public ICollection<UsersToAccountants> UsersToAccountants { get; set; }
    }
}
