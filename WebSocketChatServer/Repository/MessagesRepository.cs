﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebSocketChatServer.Entities;

namespace WebSocketChatServer.Repository
{
    public class MessagesRepository : GenericRepository<UsersMessages>, IMessagesRepository
    {
        public MessagesRepository(DbContext context) : base(context)
        {
        }

        public IQueryable<UsersMessages> GetMessagesForDialog(Guid idDialog)
        {
            return FindBy(x => x.DialogId == idDialog);
        }
    }
}
