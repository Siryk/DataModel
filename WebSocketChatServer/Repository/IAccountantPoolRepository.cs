﻿using WebSocketChatServer.Entities;

namespace WebSocketChatServer.Repository
{
    public interface IAccountantPoolRepository : IGenericRepository<AccounterPool>
    {
    }
}
