﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSocketChatServer.Entities;

namespace WebSocketChatServer.Repository
{
    public interface IUsersToAccountauntRepository : IGenericRepository<UsersToAccountants>
    {
        IQueryable<UsersToAccountants> GetAccountersForClient(Guid idClient);
        IQueryable<UsersToAccountants> GetClientForAccountants(Guid idAccounter);
    }
}
