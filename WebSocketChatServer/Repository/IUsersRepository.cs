﻿using System.Collections.Generic;
using WebSocketChatServer.Entities;

namespace WebSocketChatServer.Repository
{
    public interface IUsersRepository : IGenericRepository<Users>
    {
       // List<BridgeTable> SelectMany();
    }
}
