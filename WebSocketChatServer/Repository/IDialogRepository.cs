﻿using System;
using System.Linq;
using WebSocketChatServer.Entities;

namespace WebSocketChatServer.Repository
{
    public interface IDialogRepository : IGenericRepository<AcUserDialogs>
    {
        IQueryable<AcUserDialogs> GetOpenDialogsForAccounter(Guid idAccountant);
        IQueryable<AcUserDialogs> GetOpenDialogsUser(Guid idClient);
        IQueryable<AcUserDialogs> Get(Guid idClient, Guid idAccountant);
        IQueryable<AcUserDialogs> GetHistoryForClient(Guid idClient);
    }
}