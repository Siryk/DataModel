﻿using Microsoft.EntityFrameworkCore;
using WebSocketChatServer.Entities;

namespace WebSocketChatServer.Repository
{
    class AccountantPoolRepository : GenericRepository<AccounterPool>, IAccountantPoolRepository
    {
        public AccountantPoolRepository(DbContext context) : base(context)
        {

        }
    }
}
