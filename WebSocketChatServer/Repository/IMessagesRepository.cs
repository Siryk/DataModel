﻿using System;
using System.Linq;
using WebSocketChatServer.Entities;

namespace WebSocketChatServer.Repository
{
    public interface IMessagesRepository : IGenericRepository<UsersMessages>
    {
        IQueryable<UsersMessages> GetMessagesForDialog(Guid idDialog);
    }
}
