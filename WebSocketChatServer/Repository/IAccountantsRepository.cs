﻿using System;
using System.Linq;
using WebSocketChatServer.Entities;

namespace WebSocketChatServer.Repository
{
    public interface IAccountantsRepository : IGenericRepository<Accountants>
    {
        //IQueryable<Accountants> GetAccountersForClient(Guid idClient);
    }
}
