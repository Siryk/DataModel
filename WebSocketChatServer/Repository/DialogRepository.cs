﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebSocketChatServer.Entities;

namespace WebSocketChatServer.Repository
{
    public class DialogRepository : GenericRepository<AcUserDialogs>, IDialogRepository
    {
        public DialogRepository(DbContext context) : base(context)
        {
        }

        public IQueryable<AcUserDialogs> GetOpenDialogsForAccounter(Guid idAccountant)
        {
            return FindBy(x => x.AccountantsId== idAccountant && x.Status.Equals("Open"));
        }

        public IQueryable<AcUserDialogs> GetOpenDialogsUser(Guid idClient) //add for to name
        {
            return FindBy(x => x.UserId == idClient && x.Status.Equals("Open")); // swap to bool
        }

        public IQueryable<AcUserDialogs> Get(Guid idClient, Guid idAccountant)
        {
            return FindBy(x => x.AccountantsId == idAccountant && x.UserId== idClient && x.Status.Equals("Open"));
        }

        public IQueryable<AcUserDialogs> GetHistoryForClient(Guid idClient)
        {
            return FindBy(x => x.UserId == idClient);
        }

        //public sauri_v2Entities SauriContext => Context as sauri_v2Entities;
    }
}
