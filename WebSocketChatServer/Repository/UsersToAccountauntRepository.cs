﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebSocketChatServer.Entities;

namespace WebSocketChatServer.Repository
{
    public class UsersToAccountauntRepository : GenericRepository<UsersToAccountants> , IUsersToAccountauntRepository
    {
        public UsersToAccountauntRepository(DbContext context) : base(context)
        {

        }

        public IQueryable<UsersToAccountants> GetAccountersForClient(Guid idClient)
        {
            return FindBy(x => x.UserId.Equals(idClient));
        }

        public IQueryable<UsersToAccountants> GetClientForAccountants(Guid idAccounter)
        {
            return FindBy(x => x.AccountantId.Equals(idAccounter));
        }
    }
}
