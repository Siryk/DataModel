﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using PushSharp.Common;
using PushSharp.Google;
using WebSocketChatServer.Core;
using WebSocketChatServer.Entities;
using WebSocketChatServer.WebSockets;

namespace WebSocketChatServer.ChatAPI
{
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public enum NotificationTypes
        {
            ios,
            android,
            winMobile
        }


        public class NotificationSettings
        {
            public string DeviceToken { get; set; }

            public string AppVersion { get; set; }

            public Guid UserID;

            public NotificationTypes ReceiverType { get; set; }
        }

        public class NotificationMobile
        {

            ApnsConfiguration config;

            public static string CertPath;

            /// <summary>
            /// notify user about answer by sending mobile push notification
            /// </summary>
            /// <param name="AccUserid"></param>
            public void SendPushAnswer(Message message)
            {
                ChatManager manager = new ChatManager(new UnitOfWork(new SauriChatContext()));
                var user = manager.GetUserById(message.UserId);

                if (user != null && user.MobileNotifySettings != null)
                {
                    var ns = JsonConvert.DeserializeObject<NotificationSettings>(user.MobileNotifySettings);

                    if (ns.ReceiverType == NotificationTypes.ios)
                        Send(ns, message.MessageText);

                    if (ns.ReceiverType == NotificationTypes.android)
                        SendToAndroid(ns, message.MessageText, message.AccounterName);
                }
        }

            public void Send(NotificationSettings ns)
            {
                Send(ns, "message from server sauri beta");
            }

            public void Send(NotificationSettings ns, string message)
            {
                var path = CertPath; // System.Web.HttpContext.Current.Server.MapPath(string.Format("~/App_Data/"));

#if(DEBUG)
                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox,
                    path + "sauri apn sand and prod.p12", "36525874");
#else

             config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production,
                path + "sauri apn sand and prod.p12", "36525874");
#endif

                // Create a new broker
                var apnsBroker = new ApnsServiceBroker(config);

                // Wire up events
                apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                {
                    aggregateEx.Handle(ex =>
                    {

                        // See what kind of exception it was to further diagnose
                        if (ex is ApnsNotificationException)
                        {
                            var notificationException = (ApnsNotificationException) ex;

                            // Deal with the failed notification
                            var apnsNotification = notificationException.Notification;
                            var statusCode = notificationException.ErrorStatusCode;

                            //Console.WriteLine($"Apple Notification Failed: ID={apnsNotification.Identifier}, Code={statusCode}");
                        }
                        else
                        {
                            // Inner exception might hold more useful information like an ApnsConnectionException			
                            //Console.WriteLine($"Apple Notification Failed for some unknown reason : {ex.InnerException}");

                        }

                        // Mark it as handled
                        return true;
                    });
                };

                apnsBroker.OnNotificationSucceeded += (notification) =>
                {
                    //Console.WriteLine("Apple Notification Sent!");
                };

                // Start the broker
                apnsBroker.Start();

                var MY_DEVICE_TOKENS = new List<string>();
                MY_DEVICE_TOKENS.Add(ns.DeviceToken);

                foreach (var deviceToken in MY_DEVICE_TOKENS)
                {
                    // Queue a notification to Send
                    apnsBroker.QueueNotification(new ApnsNotification
                    {
                        DeviceToken = deviceToken,
                        Payload =
                            JObject.Parse("{\"aps\":{\"alert\":\"" + message +
                                          "\",\"badge\" : 1}}") //"{\"aps\":{\"badge\":7}}")
                    });
                }

                // Stop the broker, wait for it to finish   
                // This isn't done after every message, but after you're
                // done with the broker
                apnsBroker.Stop();
            }

            public void SendToAndroid(NotificationSettings ns, string message, string name)
            {
                // Configuration
                //    var config = new GcmConfiguration("GCM-SENDER-ID", "AUTH-TOKEN", null);
                var config = new GcmConfiguration("973003246648",
                    "AAAA4ouDTDg:APA91bE-fgfJeESP2ByUTm1lHcKab-w4Crlimp6wtxS1sNp8eG5un19TLYCOnhkGAY9v5FWC134TjvbaTil-spjRSmFSsjB9s62Qap7bNMsv4UPYV7l9F_LYSjz5WijRxg6lQsGYMugh"
                    , null);

                // Create a new broker
                var gcmBroker = new GcmServiceBroker(config);

                // Wire up events
                gcmBroker.OnNotificationFailed += (notification, aggregateEx) =>
                {
                    aggregateEx.Handle(ex =>
                    {

                        // See what kind of exception it was to further diagnose
                        if (ex is GcmNotificationException)
                        {
                            var notificationException = (GcmNotificationException) ex;

                            // Deal with the failed notification
                            var gcmNotification = notificationException.Notification;
                            var description = notificationException.Description;

                            //   Console.WriteLine($"GCM Notification Failed: ID={gcmNotification.MessageId}, Desc={description}");

                        }
                        else if (ex is GcmMulticastResultException)
                        {
                            var multicastException = (GcmMulticastResultException) ex;

                            foreach (var succeededNotification in multicastException.Succeeded)
                            {
                                //  Console.WriteLine($"GCM Notification Succeeded: ID={succeededNotification.MessageId}");
                            }

                            foreach (var failedKvp in multicastException.Failed)
                            {
                                var n = failedKvp.Key;
                                var e = failedKvp.Value;

                                //   Console.WriteLine($"GCM Notification Failed: ID={n.MessageId}, Desc={e.Description}");
                            }

                        }
                        else if (ex is DeviceSubscriptionExpiredException)
                        {
                            var expiredException = (DeviceSubscriptionExpiredException) ex;

                            var oldId = expiredException.OldSubscriptionId;
                            var newId = expiredException.NewSubscriptionId;

                            // Console.WriteLine($"Device RegistrationId Expired: {oldId}");


                            if (!string.IsNullOrWhiteSpace(newId))
                            {
                                // If this value isn't null, our subscription changed and we should update our database
                                // Console.WriteLine($"Device RegistrationId Changed To: {newId}");
                            }
                        }
                        else if (ex is RetryAfterException)
                        {
                            var retryException = (RetryAfterException) ex;
                            // If you get rate limited, you should stop sending messages until after the RetryAfterUtc date
                            //  Console.WriteLine($"GCM Rate Limited, don't Send more until after {retryException.RetryAfterUtc}");

                        }
                        else
                        {
                            // Console.WriteLine("GCM Notification Failed for some unknown reason");

                        }

                        // Mark it as handled
                        return true;
                    });
                };

                gcmBroker.OnNotificationSucceeded += (notification) =>
                {
                    // Console.WriteLine("GCM Notification Sent!");
                };

                var MY_REGISTRATION_IDS = new List<string>();
                MY_REGISTRATION_IDS.Add(ns.DeviceToken);

                // Start the broker
                gcmBroker.Start();

                foreach (var regId in MY_REGISTRATION_IDS)
                {
                    // Queue a notification to Send
                    gcmBroker.QueueNotification(new GcmNotification
                    {
                        RegistrationIds = new List<string>
                        {
                            regId
                        },
                        //Data = JObject.Parse("{\"notification\" : {\"body\" :\""+ message + "\",\"title\" : \"messageTitle\"}}")
                        Data = JObject.Parse("{\"body\" :\"" + message + "\",\"title\" : \"Sauri: " + name + "\"}")
                    });
                }

                // Stop the broker, wait for it to finish   
                // This isn't done after every message, but after you're
                // done with the broker
                gcmBroker.Stop();
            }
        }
    }
