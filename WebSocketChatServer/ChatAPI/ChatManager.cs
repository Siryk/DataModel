﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Newtonsoft.Json;
using WebSocketChatServer.Core;
using WebSocketChatServer.Entities;
using WebSocketChatServer.WebSockets;
using System.Text.RegularExpressions;
using WebSocketChatServer.ViewModel;
using WebSocketChatServer.ViewModel;

namespace WebSocketChatServer.ChatAPI
{
    [Serializable]
    public class ChatManager : IChatManager
    {
        /// <summary>
        /// Обьект для унифицированной работы с базой данных. 
        /// </summary>
        private readonly IUnitOfWork _unit;

        public ChatManager(IUnitOfWork unit)
        {
            _unit = unit;
        }

        //
        public Users GetUserById(Guid id)
        {
            return _unit.Users.Get(id);
        }
        /// <summary>
        /// Функция возврата списка для бухгалтера со всеми его открытыми диалогами и сообщениями в них.
        /// </summary>
        /// <param name="idAccounter">Ключ бухгалтера.</param>
        /// <returns>Возвращает модель данных которая включает в себя диалог -> набор сообщений в диалоге.</returns>
        public List<DialogRequestModel> GetOpenDialogsHistoryForAccounter(Guid idAccounter)
        {
            var requestModelsList = new List<DialogRequestModel>();

            var dialogs = _unit.Dialogs.GetOpenDialogsForAccounter(idAccounter);

            foreach (var dialog in dialogs)
            {
                var collectionMessagesForDialog = _unit.AllMessages
                    .GetMessagesForDialog(dialog.Id)
                    .Where(x => x.StatusMessages.Equals(StatusMessage.Accept.ToString()));

                var name = _unit.Users.Get(dialog.UserId).Name;

                requestModelsList.Add(new DialogRequestModel {
                    UserId = dialog.UserId,
                    Count = collectionMessagesForDialog.Count(),
                    DialogId = dialog.Id,
                    UserName = name.Remove(0, name.LastIndexOf("(") + 1).Trim(')'),
                    Phone = name.Substring(0,name.LastIndexOf("("))                   
                    //MessagesesList = collectionMessagesForDialog
                });
            }

            return requestModelsList;
        }

        private List<MessageModel> FormattingMessages(IQueryable<AcUserDialogs> dialogs)
        {
            List<UsersMessages> messages = new List<UsersMessages>();

            foreach (var dialog in dialogs)
            {
                messages.AddRange(_unit.AllMessages.GetMessagesForDialog(dialog.Id));
            }

            var returnList = messages.OrderByDescending(x => x.Date).ToList();

            return returnList.Select(message =>
                new MessageModel()
                {
                    MessageId = message.MessageId,
                    AuthorStatus = message.AuthorStatus,
                    MessageText = message.MessageText,
                    Date = message.Date.ToString("d MMMMM H:mm", new CultureInfo("ru-RU")),
                    StatusMessages = message.StatusMessages,
                    DialogId = message.DialogId,
                    NameInterviewee = _unit.AllAccountants.Get(_unit.Dialogs.Get(message.DialogId).AccountantsId).Name
                }).ToList();
        }
        public List<MessageModel> GetHistoryMessagesForAccounterContact(Guid idUser, Guid idAccounter)
        {
            return FormattingMessages(_unit.Dialogs.Get(idUser, idAccounter));
        }

        public List<MessageModel> GetMessagesForDialogAccounter(Guid idDialog)
        {
            return _unit.AllMessages.GetMessagesForDialog(idDialog).ToList().OrderByDescending(x => x.Date).ToList().Select(message => 
                new MessageModel()
                {
                    MessageId = message.MessageId,
                    AuthorStatus = message.AuthorStatus,
                    MessageText = message.MessageText,
                    Date = message.Date.ToString("d MMMMM H:mm", new CultureInfo("ru-RU")),
                    StatusMessages = message.StatusMessages,
                    DialogId = message.DialogId,
                    NameInterviewee = _unit.Users.Get(_unit.Dialogs.Get(message.DialogId).UserId).Name
                }
                ).ToList();
        }

        //dobavit commentariy
        public List<MessageModel> GetMessagesForUser(Guid idUsers)
        {
            return FormattingMessages(_unit.Dialogs.GetHistoryForClient(idUsers));
        }

        /// <summary>
        /// Функция нахождения связи между Пользователями и Бухгалтерами.
        /// </summary>
        /// <returns>Возвращает список со значениями ключ бухгалтера - ключ пользователя.</returns>
        public List<UsersToAccountants> GetListAccountersForClient(Guid idClient)
        {
            return _unit.BridgeRepository.GetAccountersForClient(idClient)
                .ToList(); 
        }

        public List<ContactModel> GetListClientForAccountant(Guid id)
        {
            return _unit.BridgeRepository.GetClientForAccountants(id)
                .Select(x => new ContactModel() {UserName = x.User.Name.Remove(0, x.User.Name.LastIndexOf("(") + 1).Trim(')'),
                    UserId = x.UserId,
                    Phone = x.User.Name.Substring(0, x.User.Name.LastIndexOf("(")) })
                .ToList();
        }
        /// <summary>
        /// Поиск диалога, в случае если нету такого диалог, то создает новый.
        /// </summary>
        /// <param name="idClient">Ключ пользователя.</param>
        /// <param name="idAccountant">Ключ бухгалтера.(не обьязательный параметр)</param>
        /// <returns>Ключ диалога.</returns>
        public Guid GetDialog(Guid idClient, Guid idAccountant = default(Guid))
        {
            var dialogs = idAccountant == Guid.Empty ? _unit.Dialogs.GetOpenDialogsUser(idClient) : _unit.Dialogs.Get(idClient, idAccountant);
            return dialogs.Any() ? dialogs.First().Id : CreateDialog(idClient, idAccountant);
        }
        /// <summary>
        /// В разработке(функция для определения очереди бухгалтеров).
        /// </summary>
        /// <returns>Ключ бухгалтера.</returns>
        private Guid GetAccountantFromPool()
        {
            return _unit.Pool.GetAll().FirstOrDefault().AccounterId;
        }

        /// <summary>
        /// Создание диалог в случае если диалог отсутсвует, если уже существует, то находит этот диалог.
        /// </summary>
        /// <param name="idClient">Ключ пользователя.</param>
        /// <param name="idAccountant">Ключ бухгалтера.</param>
        /// <returns>Ключ диалога.</returns>
        private Guid CreateDialog(Guid idClient, Guid idAccountant = default(Guid))
        {
            var accounter = idAccountant == Guid.Empty ? GetAccountantIdForClient(idClient) : idAccountant;

            var resultGuid = Guid.NewGuid();

                _unit.Dialogs.Add(new AcUserDialogs {
                    Id = resultGuid,
                    UserId = idClient,
                    AccountantsId = accounter,
                    Status = "Open",
                    CarrotId = "211111111",//liwnee
                    CarrotUserId = "2000000",//liwnee
                    AccountUserId = new Guid("80742918-aec4-4dfe-aec5-94ac321c6bd6")//liwnee
                });
                      
            _unit.Complete();

            return resultGuid;
        }

        /// <summary>
        /// Нахождения бухгалтера для пользователя, если он назначен для него, в случае если нет, то находит свободного из доступных.
        /// </summary>
        /// <param name="idClient">Ключ пользователя.</param>
        /// <returns>Ключ бухгалтера.</returns>
        private Guid GetAccountantIdForClient(Guid idClient)
        {
            var accounters = _unit.BridgeRepository.GetAccountersForClient(idClient);
            //AllAccountants.GetAccountersForClient(idClient) ;
            return accounters.Any() ? accounters.First().AccountantId : GetAccountantFromPool();
        }

        /// <summary>
        /// Функция для закрытия диалога.(В разработке).
        /// </summary>
        /// <param name="idDialog"></param>
        public bool CloseDialog(Guid idDialog)
        {
            var dialog = _unit.Dialogs.Get(idDialog);
            dialog.Status = "Close";
            _unit.Dialogs.Update(dialog);
            return Convert.ToBoolean(_unit.Complete());
        }

        /// <summary>
        /// Создание сообщения для занесения в базу данных.
        /// </summary>
        /// <param name="idDialog">Ключ диалога.</param>
        /// <param name="authorStatus">Флаг для определения кто занес сообщение в БД (Либо пользователь, либо бухгалтер).</param>
        /// <param name="textMessage">Сообщение.</param>
        /// <param name="messageStatus">Тип сообщения</param>
        /// <returns>Ключ сообщения.</returns>
        public async Task CreateMessage(Guid idDialog, bool authorStatus, string textMessage, string messageStatus, DateTime time)
        {
            var messageGuid = Guid.NewGuid();

            _unit.AllMessages.Add(new UsersMessages
            {
                DialogId = idDialog,
                AuthorStatus = authorStatus,
                Date = time,
                StatusMessages = messageStatus,
                MessageText = textMessage,
                MessageId = messageGuid
            });

           await _unit.CompleteAsync();
        }
        /// <summary>
        /// comment
        /// </summary>
        /// <param name="idDialog"></param>
        /// <returns></returns>
        public Guid UserIdInDialog(Guid idDialog)
        {
            return (Guid)_unit.Dialogs.FindBy(x => x.Id == idDialog).First().UserId;
        }
        /// <summary>
        /// koment
        /// </summary>
        /// <param name="idDialog"></param>
        /// <returns></returns>
        public Guid AccounterIdInDialog(Guid idDialog)
        {
            return (Guid) _unit.Dialogs.FindBy(x => x.Id == idDialog).First().AccountantsId;
        }
        /// <summary>
        /// Создание рассылки для бухгалтеров.
        /// </summary>
        /// <param name="listForClient">Список ключей пользователей.</param>
        /// <param name="idAccounter">Ключ бухгалтера.</param>
        /// <returns>Список диалогов куда отправлять.</returns>
        public List<Guid> CreateDialogsForAccountant(List<Guid> listForClient, Guid idAccounter)
        {
            var dialogsId = new List<Guid>();

            foreach (var user in listForClient)
            {
                dialogsId.Add(GetDialog(user, idAccounter));
            }

            return dialogsId;

        }
    }
}
