﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebSocketChatServer.Entities;
using WebSocketChatServer.ViewModel;

namespace WebSocketChatServer.ChatAPI
{
    public interface IChatManager
    {
        List<DialogRequestModel> GetOpenDialogsHistoryForAccounter(Guid idAccounter);
        List<UsersToAccountants> GetListAccountersForClient(Guid idClient);
        Guid GetDialog(Guid idClient, Guid idAccountant);
        bool CloseDialog(Guid idDialog);
        Task CreateMessage(Guid idDialog, bool authorStatus, string textMessage, string messageStatus,DateTime time);
        List<Guid> CreateDialogsForAccountant(List<Guid> listForClient, Guid idAccounter);
        Guid AccounterIdInDialog(Guid idDialog);
    }
}
