﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebSocketChatServer.ChatAPI;
using WebSocketChatServer.Core;
using WebSocketChatServer.Entities;
using Newtonsoft.Json;
using WebSocketChatServer.ViewModel;

namespace WebSocketChatServer.Controllers
{
   // [Produces("application/json")]
   // [EnableCors("AllowSpecificOrigin")]
    public class ChatController : Controller
    {
        private readonly ChatManager _manager;
        private readonly IConfiguration _configuration;
        
        public ChatController(IConfiguration config)
        {
            _configuration = config;
            _manager = new ChatManager(new UnitOfWork(new SauriChatContext()));
        }

        [Route("api/chat/${id}")]
        public ActionResult GetChat(string id)
        {
            return View((object) id);
        }

        [Route(("api/chataccounter/${id}"))]
        public ActionResult GetChatAccounter(string id)
        {
            return View((object) id);
        }
        [Route("/accounter/${id}")]
        public ActionResult GetOpenDialogsForAccounter(Guid id)
        {
            return Json(JsonConvert.SerializeObject(_manager.GetOpenDialogsHistoryForAccounter(id)));
        }

        [HttpGet]
        [Route("/users/${id}${index}")]
        public ActionResult GetMessagesForUser(Guid id,int index)
        {
            var returnList = CheckRangeForUserMessages(id, index);
            return Json(JsonConvert.SerializeObject(returnList));
        }

        //perepisat takoe sebe ne krasivo????rabotaet s ogranicheniem 
        private List<MessageModel> CheckRangeForUserMessages(Guid id, int index)
        {
            var returnList = _manager.GetMessagesForUser(id);

            return (index < 0) || (returnList.Count - index < 0)
                ? Enumerable.Empty<MessageModel>().ToList()
                : returnList.Skip(index).Take(20).ToList();
        }

        [Route("/dialog/${idDialog}")]
        public ActionResult GetMessagesForDialog(Guid idDialog)
        {
            return Json(JsonConvert.SerializeObject(_manager.GetMessagesForDialogAccounter(idDialog)));
        }

        [HttpGet]
        [Route("/view/${id}${index}")]
        public ActionResult ChatViewSauri(Guid id,int index)
        {
            var returnList = CheckRangeForUserMessages(id, index);
            return View(returnList);
        }

        [HttpGet]
        [Route("/contact/${id}")]
        public ActionResult AllContactForAccounter(Guid id)
        {
            return Json(JsonConvert.SerializeObject(_manager.GetListClientForAccountant(id)));
        }

        [HttpGet]
        [Route("/contactmessage/${idUser}${idAccounter}")]
        public ActionResult MessageForContactAccounter(Guid idUser, Guid idAccounter)
        {
            return Json(
                JsonConvert.SerializeObject(_manager.GetHistoryMessagesForAccounterContact(idUser, idAccounter)));
        }
    }
}