
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/18/2017 16:06:20
-- Generated from EDMX file: D:\Repository\DataModel\DataModel\Model\ProgramModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [sauri_v2];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[sauri_v2ModelStoreContainer].[FK__UsersMess__Autho__19DFD96B]', 'F') IS NOT NULL
    ALTER TABLE [sauri_v2ModelStoreContainer].[UsersMessages] DROP CONSTRAINT [FK__UsersMess__Autho__19DFD96B];
GO
IF OBJECT_ID(N'[sauri_v2ModelStoreContainer].[FK__UsersToAc__Accou__17036CC0]', 'F') IS NOT NULL
    ALTER TABLE [sauri_v2ModelStoreContainer].[UsersToAccountants] DROP CONSTRAINT [FK__UsersToAc__Accou__17036CC0];
GO
IF OBJECT_ID(N'[sauri_v2ModelStoreContainer].[FK__UsersToAc__UserI__17F790F9]', 'F') IS NOT NULL
    ALTER TABLE [sauri_v2ModelStoreContainer].[UsersToAccountants] DROP CONSTRAINT [FK__UsersToAc__UserI__17F790F9];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Accountants]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Accountants];
GO
IF OBJECT_ID(N'[dbo].[AcUserDialogs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AcUserDialogs];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[sauri_v2ModelStoreContainer].[UsersMessages]', 'U') IS NOT NULL
    DROP TABLE [sauri_v2ModelStoreContainer].[UsersMessages];
GO
IF OBJECT_ID(N'[sauri_v2ModelStoreContainer].[UsersToAccountants]', 'U') IS NOT NULL
    DROP TABLE [sauri_v2ModelStoreContainer].[UsersToAccountants];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Accountants'
CREATE TABLE [dbo].[Accountants] (
    [ID] uniqueidentifier  NOT NULL,
    [Name] nvarchar(200)  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [ID] uniqueidentifier  NOT NULL,
    [Name] nvarchar(200)  NOT NULL,
    [CarrotId] nvarchar(200)  NOT NULL,
    [ConsultantID] uniqueidentifier  NULL,
    [MobileNotifySettings] nvarchar(500)  NULL
);
GO

-- Creating table 'AcUserDialogs'
CREATE TABLE [dbo].[AcUserDialogs] (
    [ID] uniqueidentifier  NOT NULL,
    [CarrotID] nvarchar(100)  NOT NULL,
    [AccountUserID] uniqueidentifier  NOT NULL,
    [CarrotUserID] nvarchar(100)  NOT NULL,
    [Status] nvarchar(100)  NOT NULL,
    [UsersID] uniqueidentifier  NOT NULL,
    [AccountantsID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'UsersMessages'
CREATE TABLE [dbo].[UsersMessages] (
    [MessageId] uniqueidentifier  NOT NULL,
    [DialogId] uniqueidentifier  NOT NULL,
    [StatusMessages] bit  NOT NULL,
    [MessageName] datetime  NOT NULL,
    [AuthorStatus] bit  NOT NULL
);
GO

-- Creating table 'UsersToAccountants'
CREATE TABLE [dbo].[UsersToAccountants] (
    [Accountants_ID] uniqueidentifier  NOT NULL,
    [Users_ID] uniqueidentifier  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'Accountants'
ALTER TABLE [dbo].[Accountants]
ADD CONSTRAINT [PK_Accountants]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'AcUserDialogs'
ALTER TABLE [dbo].[AcUserDialogs]
ADD CONSTRAINT [PK_AcUserDialogs]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [MessageId], [DialogId], [StatusMessages], [MessageName], [AuthorStatus] in table 'UsersMessages'
ALTER TABLE [dbo].[UsersMessages]
ADD CONSTRAINT [PK_UsersMessages]
    PRIMARY KEY CLUSTERED ([MessageId], [DialogId], [StatusMessages], [MessageName], [AuthorStatus] ASC);
GO

-- Creating primary key on [Accountants_ID], [Users_ID] in table 'UsersToAccountants'
ALTER TABLE [dbo].[UsersToAccountants]
ADD CONSTRAINT [PK_UsersToAccountants]
    PRIMARY KEY CLUSTERED ([Accountants_ID], [Users_ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Accountants_ID] in table 'UsersToAccountants'
ALTER TABLE [dbo].[UsersToAccountants]
ADD CONSTRAINT [FK_UsersToAccountants_Accountants]
    FOREIGN KEY ([Accountants_ID])
    REFERENCES [dbo].[Accountants]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Users_ID] in table 'UsersToAccountants'
ALTER TABLE [dbo].[UsersToAccountants]
ADD CONSTRAINT [FK_UsersToAccountants_Users]
    FOREIGN KEY ([Users_ID])
    REFERENCES [dbo].[Users]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersToAccountants_Users'
CREATE INDEX [IX_FK_UsersToAccountants_Users]
ON [dbo].[UsersToAccountants]
    ([Users_ID]);
GO

-- Creating foreign key on [DialogId] in table 'UsersMessages'
ALTER TABLE [dbo].[UsersMessages]
ADD CONSTRAINT [FK__UsersMess__Autho__19DFD96B]
    FOREIGN KEY ([DialogId])
    REFERENCES [dbo].[AcUserDialogs]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__UsersMess__Autho__19DFD96B'
CREATE INDEX [IX_FK__UsersMess__Autho__19DFD96B]
ON [dbo].[UsersMessages]
    ([DialogId]);
GO

-- Creating foreign key on [UsersID] in table 'AcUserDialogs'
ALTER TABLE [dbo].[AcUserDialogs]
ADD CONSTRAINT [FK_UsersAcUserDialogs]
    FOREIGN KEY ([UsersID])
    REFERENCES [dbo].[Users]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersAcUserDialogs'
CREATE INDEX [IX_FK_UsersAcUserDialogs]
ON [dbo].[AcUserDialogs]
    ([UsersID]);
GO

-- Creating foreign key on [AccountantsID] in table 'AcUserDialogs'
ALTER TABLE [dbo].[AcUserDialogs]
ADD CONSTRAINT [FK_AccountantsAcUserDialogs]
    FOREIGN KEY ([AccountantsID])
    REFERENCES [dbo].[Accountants]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountantsAcUserDialogs'
CREATE INDEX [IX_FK_AccountantsAcUserDialogs]
ON [dbo].[AcUserDialogs]
    ([AccountantsID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------