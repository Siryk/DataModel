﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace DataModel.Repository
{
    public interface IRepository<T> : IDisposable where T : class
    {
        T Get(int id);
        void Add(T entity);
        void Remove(T entity);
        void Update(T entity);
        void Save();
        IEnumerable<T> GetAll();

    }
}
