﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using DataInfrastructure.Model;
using DataInfrastructure.Repository;
using FizzWare.NBuilder;

namespace DataTest.RepositoryTest
{
    [TestClass]
    public class MessageRepositoryTest
    {
        private IMessagesRepository _messagesRepository;

        [TestInitialize]
        public void UserRepositoryTestSetup()
        {
            _messagesRepository = new MessagesRepository(new sauri_v2Entities());
        }

        [TestMethod]
        public void DatabaseConnectionTest()
        {
            Assert.AreNotEqual(new sauri_v2Entities().Database.Connection, null);
        }

        [TestMethod]
        [Priority(0)]
        public void InsetToUsersMessagesTableTest()
        {
            StaticValue.ListMessages.AddRange(Builder<UsersMessages>.CreateListOfSize(10).All()
                .With(x => x.MessageId = Guid.NewGuid())
                .With(x => x.DialogId = Guid.NewGuid())
                .With(x => x.MessageText = Faker.Lorem.Paragraph())
                .With(x => x.Date = DateTime.Now)
                .With(x => x.StatusMessages = true)
                .With(x => x.AuthorStatus = true)
                .Build().ToList());

            foreach (var message in StaticValue.ListMessages)
            {
                _messagesRepository.Add(message);
            }

            int result = _messagesRepository.Save();

            Assert.AreNotEqual(0, result);
        }

        [TestMethod]
        [Priority(3)]
        public void UpdateGetByMessage()
        {
            var messages = _messagesRepository.Get(StaticValue.ListMessages.Last().MessageId);
            messages.MessageText = Faker.Lorem.Sentence();
            _messagesRepository.Update(messages);
            _messagesRepository.Save();
            var result = _messagesRepository.FindBy(x => x.MessageId == messages.MessageId).First();
            Assert.AreEqual(messages.MessageText, result.MessageText);
            StaticValue.ListMessages.Last().MessageText = messages.MessageText;
        }

        [TestMethod]
        [Priority(4)]
        public void DeleteMessagesTest()
        {
            foreach (var message in StaticValue.ListMessages)
            {
                _messagesRepository.Delete(message);
            }

            int result = _messagesRepository.Save();
            Assert.AreNotEqual(0, result);
        }
    }
}

