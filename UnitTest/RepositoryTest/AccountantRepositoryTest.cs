﻿using System;
using System.Linq;
using DataInfrastructure.Model;
using DataInfrastructure.Repository;
using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataTest.RepositoryTest
{
    [TestClass]
    public class AccountantRepositoryTest
    {
        public IAccountantsRepository AccountantRepository { get; set; }

        [TestInitialize]
        public void UserRepositoryTestSetup()
        {
            AccountantRepository = new AccountantsRepository(new sauri_v2Entities());
        }

        [TestMethod]
        [Priority(0)]
        public void DatabaseConnectionTest()
        {
            Assert.AreNotEqual(new sauri_v2Entities().Database.Connection, null);
        }

        [TestMethod]
        [Priority(0)]
        public void InsetToAccountantTableTest()
        {
            StaticValue.ListAccountants.AddRange(Builder<Accountants>.CreateListOfSize(10).All()
                .With(x => x.ID = Guid.NewGuid())
                .With(x => x.Name = Faker.Name.FullName())
                .Build().ToList());

            foreach (var accountant in StaticValue.ListAccountants)
            {
                AccountantRepository.Add(accountant);
            }

            int result = AccountantRepository.Save();

            Assert.AreNotEqual(0, result);

        }

        [Priority(2)]
        public Accountants GetAccounter(Guid id)
        {
            return AccountantRepository.Get(id);
        }
        [TestMethod]
        [Priority(3)]
        public void DeleteTestForAccountant()
        {
            foreach (var accountant in StaticValue.ListAccountants)
            {
                AccountantRepository.Delete(accountant);
            }

            int result = AccountantRepository.Save();

            Assert.AreNotEqual(0, result);
        }
    }
}
