﻿using System;
using System.Collections.Generic;
using DataInfrastructure.Model;
using DataInfrastructure.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using FizzWare.NBuilder;
using Faker;
using FizzWare.NBuilder.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataTest.RepositoryTest
{
    public static class StaticValue{
        public static List<Users> ListUsers { get; set; } = new List<Users>();
        public static List<UsersMessages> ListMessages { get; set; } = new List<UsersMessages>();
        public static List<AcUserDialogs> ListDialogs { get; set; } = new List<AcUserDialogs>();
        public static List<AccounterPool> ListPools { get; set; } = new List<AccounterPool>();
        public static List<Accountants> ListAccountants { get; set; } = new List<Accountants>();
    }

    [TestClass]
    public class UserRepositoryTest
    {
        //static List<Users> _valueList = new List<Users>();

        private IUsersRepository usersRepository ;

        [TestInitialize]
        public void UserRepositoryTestSetup()
        {
            usersRepository = new UsersRepository(new sauri_v2Entities());
            //_valueList = new List<Users>();
        }
        [TestMethod]
        [Priority(0)]
        public void DatabaseConnectionTest()
        {
            Assert.AreNotEqual(new sauri_v2Entities().Database.Connection, null);
        }

        [TestMethod]
        [Priority(0)]
        public void InsetToUsersTableTest()
        {
            StaticValue.ListUsers.AddRange(Builder<Users>.CreateListOfSize(10).All()
                .With(x => x.ID = Guid.NewGuid())
                .With(x => x.Name = $"{Faker.Phone.Number()}({Faker.Name.FullName()})")
                .With(x => x.CarrotId = Faker.RandomNumber.Next(100000000, 999999999).ToString())
                .With(x => x.ConsultantID = null).Build().ToList());

                foreach (var user in StaticValue.ListUsers)
                {
                    usersRepository.Add(user);
                }

                int result = usersRepository.Save();

                Assert.AreNotEqual(0, result);  
        }

        [TestMethod]
        [Priority(1)]
        public void SelectAllTest()
        {
                int result = usersRepository.GetAll().Count();
                Assert.AreNotEqual(0,result);
            
        }

        [TestMethod]
        [Priority(2)]
        public void UpdateGetByTest()
        {
            var user = usersRepository.Get(StaticValue.ListUsers.Last().ID);
            user.Name = Faker.Name.FullName();
            usersRepository.Update(user);
            usersRepository.Save();
            var result = usersRepository.FindBy(x => x.ID == user.ID).First();
            Assert.AreEqual(user.Name, result.Name);
            StaticValue.ListUsers.Last().Name = user.Name;
        }

        [TestMethod]
        [Priority(3)]
        public void DeleteTest()
        {
                foreach (var user in StaticValue.ListUsers)
                {
                    usersRepository.Delete(user);
                }

                int result = usersRepository.Save();
                Assert.AreNotEqual(0,result);
        }
    }
}
