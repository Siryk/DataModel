﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataCore
{
    public class User 
    {
        [Required]
        public Guid ID { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        [MaxLength(200)]
        public string CarrotId { get; set; }

        public Nullable<System.Guid> ConsultantID { get; set; }

        [MaxLength(500)]
        public string MobileNotifySettings { get; set; }
    }
}
