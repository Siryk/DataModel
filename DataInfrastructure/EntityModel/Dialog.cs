﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataCore.EntityModel
{
    public class Dialog
    {
        [Required]
        public Guid ID { get; set; }

        [Required]
        [MaxLength(100)]
        public string CarrotID { get; set; }

        [Required]
        public Guid AccountUserID { get; set; }

        [Required]
        [MaxLength(100)]
        public string CarrotUserID { get; set; }

        [Required]
        [MaxLength(100)]
        public string Status { get; set; }
    }
}
