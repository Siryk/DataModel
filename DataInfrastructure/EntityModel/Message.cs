﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataCore.EntityModel
{
    public class Message
    {
        [Required]
        public Guid MessageId { get; set; }
        
        [Required]
        public Guid DialogId { get; set; }

        [Required]
        public bool StatusMessages { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public bool AuthorStatus { get; set; }

        public string MessageText { get; set; }
    }
}
