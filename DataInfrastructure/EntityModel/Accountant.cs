﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataCore.EntityModel
{
    public class Accountant
    {
        [Required]
        public Guid ID { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
    }
}
