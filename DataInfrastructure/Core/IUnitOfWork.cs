﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataInfrastructure.Repository;

namespace DataInfrastructure.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IUsersRepository Users { get; }
        IMessagesRepository AllMessages { get; }
        IDialogRepository Dialogs { get; }
        IAccountantsRepository AllAccountants { get; }
        IAccountantPoolRepository Pool { get; }
        int Complete();
        Task<int> CompleteAsync();
    }
}
