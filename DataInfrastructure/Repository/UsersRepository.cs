﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataCore.EntityModel;
using DataCore;
using DataInfrastructure.Model;
using System.Data.Entity.Core;
using System.Linq.Expressions;
using System.Runtime.Remoting.Messaging;

namespace DataInfrastructure.Repository
{
    public class BridgeTable
    {
        public BridgeTable(Guid userId, Guid accountId)
        {
            UserId = userId;
            AccountId = accountId;
        }
        public BridgeTable()
        {

        }

        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }
    }

    public class UsersRepository : GenericRepository<Users> , IUsersRepository
    {
        public UsersRepository(DbContext context) : base(context)
        {
        }

        public List<BridgeTable> SelectMany() //Expression<Func<Users, bool>> predicate
        {
            return SauriContext.Users.SelectMany(x => x.Accountants, (em, prop) => new BridgeTable{AccountId = prop.ID, UserId = em.ID}).ToList();
        }

        public void AddReleationshipToAccountant(Guid idUsers, Guid idAccounter)
        {
            var user = Get(idUsers);
            user.Accountants.Add(SauriContext.Accountants.Single(x => x.ID == idAccounter));
            Save();
        }

        public sauri_v2Entities SauriContext => Context as sauri_v2Entities;
    }
}
  