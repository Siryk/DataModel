﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DataInfrastructure.Model;
using System.Data.Entity;

namespace DataInfrastructure.Repository
{
    public class MessagesRepository : GenericRepository<UsersMessages>, IMessagesRepository
    {
        public MessagesRepository(DbContext context) : base(context)
        {
        }

        public IQueryable<UsersMessages> GetMessagesForDialog(Guid idDialog)
        {
            return FindBy(x => x.DialogId == idDialog);
        }
    }
}
