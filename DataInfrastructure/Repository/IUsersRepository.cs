﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataInfrastructure.Model;

namespace DataInfrastructure.Repository
{
    public interface IUsersRepository : IGenericRepository<Users>
    {
        List<BridgeTable> SelectMany();
    }
}
