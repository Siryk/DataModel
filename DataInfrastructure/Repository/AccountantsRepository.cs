﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataInfrastructure.Model;
using System.Data.Entity;
namespace DataInfrastructure.Repository
{
    public class AccountantsRepository : GenericRepository<Accountants> , IAccountantsRepository
    {
        public AccountantsRepository(DbContext context) : base(context)
        {

        }
        public IQueryable<Accountants> GetAccountersForClient(Guid idClient)
        {
            return FindBy(accounter => accounter.Users.Any(user => user.ID == idClient));
        }
    }
}
