﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataInfrastructure.Model;
using System.Data.Entity;

namespace DataInfrastructure.Repository
{
    public class DialogRepository : GenericRepository<AcUserDialogs>, IDialogRepository
    {
        public DialogRepository(DbContext context) : base(context)
        {
        }

        public IQueryable<AcUserDialogs> GetOpenDialogsForAccounter(Guid idAccountant)
        {
            return FindBy(x => x.AccountantsID == idAccountant && x.Status.Equals("Open"));
        }

        public IQueryable<AcUserDialogs> GetOpenDialogsUser(Guid idClient) //add for to name
        {
            return FindBy(x => x.UserID == idClient && x.Status.Equals("Open")); // swap to bool
        }

        public IQueryable<AcUserDialogs> Get(Guid idClient, Guid idAccountant)
        {
            return FindBy(x => x.AccountantsID == idAccountant && x.UserID == idClient && x.Status.Equals("Open"));
        }

        public sauri_v2Entities SauriContext => Context as sauri_v2Entities;
    }
}
