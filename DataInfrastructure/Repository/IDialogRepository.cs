﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataInfrastructure.Model;

namespace DataInfrastructure.Repository
{
    public interface IDialogRepository : IGenericRepository<AcUserDialogs>
    {
        IQueryable<AcUserDialogs> GetOpenDialogsForAccounter(Guid idAccountant);
        IQueryable<AcUserDialogs> GetOpenDialogsUser(Guid idClient);
        IQueryable<AcUserDialogs> Get(Guid idClient, Guid idAccountant);
    }
}