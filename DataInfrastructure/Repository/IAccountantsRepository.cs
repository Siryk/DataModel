﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataInfrastructure.Model;

namespace DataInfrastructure.Repository
{
    public interface IAccountantsRepository : IGenericRepository<Accountants>
    {
        IQueryable<Accountants> GetAccountersForClient(Guid idClient);
    }
}
