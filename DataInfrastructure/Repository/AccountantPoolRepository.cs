﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataInfrastructure.Model;
using System.Data.Entity;

namespace DataInfrastructure.Repository
{
    class AccountantPoolRepository : GenericRepository<AccounterPool>, IAccountantPoolRepository
    {
        public AccountantPoolRepository(DbContext context) : base(context)
        {

        }
    }
}
