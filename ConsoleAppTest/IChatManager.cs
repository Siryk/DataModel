﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAPI
{
    public interface IChatManager
    {
        List<DialogRequestModel> GetOpenDialogsHistoryForAccounter(Guid idAccounter);
        dynamic GetListAccountersForClient();
        Guid GetDialog(Guid idClient, Guid idAccountant);
        bool CloseDialog(Guid idDialog);
        Guid CreateMessage(Guid idDialog, bool authorStatus, string textMessage);
        List<Guid> CreateDialogsForAccountant(List<Guid> listForClient, Guid idAccounter);
    }
}
