﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataInfrastructure.Core;
using DataInfrastructure.Model;

namespace ChatAPI
{
    public class DialogRequestModel
    {
        public Guid DialogId { get; set; }
        public List<UsersMessages> MessagesesList { get; set; } = new List<UsersMessages>();
        public int Count { get; set; }
    }
    public class ChatManager : IChatManager
    {
        /// <summary>
        /// Обьект для унифицированной работы с базой данных. 
        /// </summary>
        private readonly IUnitOfWork _unit;

        public ChatManager(IUnitOfWork unit)
        {
            _unit = unit;
        }

        /// <summary>
        /// Функция возврата списка для бухгалтера со всеми его открытыми диалогами и сообщениями в них.
        /// </summary>
        /// <param name="idAccounter">Ключ бухгалтера.</param>
        /// <returns>Возвращает модель данных которая включает в себя диалог -> набор сообщений в диалоге.</returns>
        public List<DialogRequestModel> GetOpenDialogsHistoryForAccounter(Guid idAccounter)
        {
            var requestModelsList = new List<DialogRequestModel>();

            var dialogs = _unit.Dialogs.GetOpenDialogsForAccounter(idAccounter);

            foreach (var dialog in dialogs)
            {
                var collectionMessagesForDialog = _unit.AllMessages.GetMessagesForDialog(dialog.ID).ToList();

                requestModelsList.Add(new DialogRequestModel {
                    Count = collectionMessagesForDialog.Count,
                    DialogId = dialog.ID,
                    MessagesesList = collectionMessagesForDialog
                });
            }

            return requestModelsList;
        }

        /// <summary>
        /// Функция нахождения связи между Пользователями и Бухгалтерами.
        /// </summary>
        /// <returns>Возвращает список со значениями ключ бухгалтера - ключ пользователя.</returns>
        public dynamic GetListAccountersForClient()
        {
            return _unit.Users.SelectMany();
        }

        /// <summary>
        /// Поиск диалога, в случае если нету такого диалог, то создает новый.
        /// </summary>
        /// <param name="idClient">Ключ пользователя.</param>
        /// <param name="idAccountant">Ключ бухгалтера.(не обьязательный параметр)</param>
        /// <returns>Ключ диалога.</returns>
        public Guid GetDialog(Guid idClient, Guid idAccountant = default(Guid))
        {
            var dialogs = idAccountant == Guid.Empty ? _unit.Dialogs.GetOpenDialogsUser(idClient) : _unit.Dialogs.Get(idClient, idAccountant);
            return dialogs.Any() ? dialogs.First().ID : CreateDialog(idClient, idAccountant);
        }
        /// <summary>
        /// В разработке(функция для определения очереди бухгалтеров).
        /// </summary>
        /// <returns>Ключ бухгалтера.</returns>
        private Guid GetAccountantFromPool()
        {
            return _unit.Pool.GetAll().FirstOrDefault().AccounterId;
        }

        /// <summary>
        /// Создание диалог в случае если диалог отсутсвует, если уже существует, то находит этот диалог.
        /// </summary>
        /// <param name="idClient">Ключ пользователя.</param>
        /// <param name="idAccountant">Ключ бухгалтера.</param>
        /// <returns>Ключ диалога.</returns>
        private Guid CreateDialog(Guid idClient, Guid idAccountant = default(Guid))
        {
            var accounter = idAccountant == Guid.Empty? GetAccountantIdForClient(idClient) : idAccountant;

            var resultGuid = Guid.NewGuid();

                _unit.Dialogs.Add(new AcUserDialogs {
                    ID = resultGuid,
                    UserID = idClient,
                    AccountantsID = accounter,
                    Status = "Open",
                    CarrotID = "211111111",//liwnee
                    CarrotUserID = "2000000",//liwnee
                    AccountUserID = new Guid("80742918-aec4-4dfe-aec5-94ac321c6bd6")//liwnee
                });
                      
            _unit.Complete();

            return resultGuid;
        }

        /// <summary>
        /// Нахождения бухгалтера для пользователя, если он назначен для него, в случае если нет, то находит свободного из доступных.
        /// </summary>
        /// <param name="idClient">Ключ пользователя.</param>
        /// <returns>Ключ бухгалтера.</returns>
        private Guid GetAccountantIdForClient(Guid idClient)
        {
            var accounters = _unit.AllAccountants.GetAccountersForClient(idClient) ;
            return accounters.Any() ? accounters.First().ID : GetAccountantFromPool();
        }

        /// <summary>
        /// Функция для закрытия диалога.(В разработке).
        /// </summary>
        /// <param name="idDialog"></param>
        public bool CloseDialog(Guid idDialog)
        {
            var dialog = _unit.Dialogs.Get(idDialog);
            dialog.Status = "Close";
            _unit.Dialogs.Update(dialog);
            return Convert.ToBoolean(_unit.Complete());
        }

        /// <summary>
        /// Создание сообщения для занесения в базу данных.
        /// </summary>
        /// <param name="idDialog">Ключ диалога.</param>
        /// <param name="authorStatus">Флаг для определения кто занес сообщение в БД (Либо пользователь, либо бухгалтер).</param>
        /// <param name="textMessage">Сообщение.</param>
        /// <returns>Ключ сообщения.</returns>
        public Guid CreateMessage(Guid idDialog, bool authorStatus, string textMessage)
        {
            var messageGuid = new Guid();

            _unit.AllMessages.Add(new UsersMessages
            {
                DialogId = idDialog,
                AuthorStatus = authorStatus,
                Date = DateTime.Now,
                MessageText = textMessage,
                MessageId = messageGuid
            });

            _unit.CompleteAsync();

            return messageGuid;
        }

        /// <summary>
        /// Создание рассылки для бухгалтеров.
        /// </summary>
        /// <param name="listForClient">Список ключей пользователей.</param>
        /// <param name="idAccounter">Ключ бухгалтера.</param>
        /// <returns>Список диалогов куда отправлять.</returns>
        public List<Guid> CreateDialogsForAccountant(List<Guid> listForClient, Guid idAccounter)
        {
            var dialogsId = new List<Guid>();

            foreach (var user in listForClient)
            {
                dialogsId.Add(GetDialog(user, idAccounter));
            }

            return dialogsId;

        }
    }
}
